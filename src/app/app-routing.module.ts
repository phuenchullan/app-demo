import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DASHBOARD_ROUTES } from './components/dashboard/dashboard.routes';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
   { path: "", component: LoginComponent },
   {
      path: "dashboard",
      component: DashboardComponent,
      children: DASHBOARD_ROUTES,
      canActivate: [AuthGuardService]
   },
   { path: "**", pathMatch: "full", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
