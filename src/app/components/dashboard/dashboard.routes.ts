import { Routes } from "@angular/router";
import { HomeComponent } from "../home/home.component";
import { HeroesComponent } from "../heroes/heroes.component";
import { AddHeroComponent } from "../add-hero/add-hero.component";
import { EditHeroComponent } from "../edit-hero/edit-hero.component";
import { CurrencyComponent } from "../currency/currency.component";
import { ViewHeroComponent } from "../view-hero/view-hero.component";

export const DASHBOARD_ROUTES: Routes = [
   { path: "", component: HomeComponent },
   { path: "heroes", component: HeroesComponent },
   { path: "addHero", component: AddHeroComponent },
   { path: "editHero/:id", component: EditHeroComponent },
   { path: "viewHero/:id", component: ViewHeroComponent },
   { path: "currencies", component: CurrencyComponent },
   { path: "**", pathMatch: "full", redirectTo: "" }
];
