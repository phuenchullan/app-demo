import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor( public auth: AuthService, public router: Router ) { }

  ngOnInit() {
    if(this.auth.isAuthenticated()){
       console.log(this.auth.user);
    }
  }
   logout(){
      /* this.auth.logout().then( resolve => {
         $("#logoutModal .close").click();
         this.auth.setAuthenticate(false);
         this.router.navigate(['/']);
      }); */
      this.auth.logout();
      setTimeout(() => {
         this.auth.setAuthenticate(false);
         if (!this.auth.isAuthenticated()) {
            $("#logoutModal .close").click();
            this.router.navigate(['/']);
         }
      }, 2000);
  }

}
