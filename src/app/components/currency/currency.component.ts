import { Component, OnInit, OnDestroy } from '@angular/core';
import { CurrencyService } from '../../services/currency.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css']
})
export class CurrencyComponent implements OnInit, OnDestroy {

  dolar: any;
  euro: any;
  uf: any;

  constructor( public crs: CurrencyService ) {

  }

  ngOnInit() {
     //voy a traer los cambios de la moneda pasada por parametro
     this.crs.getCurrency("dolar").subscribe( resolve => {
         this.dolar = resolve;
     });
     this.crs.getCurrency("euro").subscribe( resolve => {
         this.euro = resolve;
     } );
     this.crs.getCurrency("uf").subscribe(resolve => {
        this.uf = resolve;
     });
  }

  ngOnDestroy(): void {
     // Metodo para limpiar memoria, buffers, etc.

  }




}
