import { Component, OnInit, Input } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';

@Component({
   selector: "app-heroetarjeta",
   templateUrl: "./heroetarjeta.component.html",
   styleUrls: ["./heroetarjeta.component.css"]
})
export class HeroetarjetaComponent implements OnInit {
   @Input() heroe: any = {};

   constructor(private fb: FirebaseService) {}

   ngOnInit() {
      console.log(this.heroe);
   }

   deleteHeroe(id: string) {
      this.fb.deleteHero(id).then(res => {
         console.log("Heroe eliminado correctamente");
      });
   }
}
