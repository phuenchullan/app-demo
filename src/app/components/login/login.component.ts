import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  constructor( public auth: AuthService, public router: Router ) {

  }

  ngOnInit() {

  }

   signup(){
      this.auth.login();
      setTimeout(() => {
         if (this.auth.isAuthenticated()) {
            this.router.navigate(['dashboard/']);
         }
      }, 3500);
   }

}
