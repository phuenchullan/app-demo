import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseService } from '../../services/firebase.service';

@Component({
  selector: 'app-edit-hero',
  templateUrl: './edit-hero.component.html',
  styleUrls: ['./edit-hero.component.css']
})
export class EditHeroComponent implements OnInit {
  id:string;
  heroe: Object = {};
  heroeupdate:any;
  private heroes: any[];
  constructor(private route: ActivatedRoute, private fb: FirebaseService, private router: Router ) {
     this.id = this.route.snapshot.params['id'];
     this.fb.getHero(this.id).subscribe(data => {
        this.heroe = data;
     });
   }

   ngOnInit() {
      console.log(this.heroes);
  }

  updateHero(){
     this.fb.updateHero(this.id, this.heroe).then( res => {
       this.router.navigate(['dashboard/heroes']);
     });
  }

}
