import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Heroe } from '../../interfaces/heroe';
import { FirebaseService } from '../../services/firebase.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-view-hero',
  templateUrl: './view-hero.component.html',
  styleUrls: ['./view-hero.component.css']
})
export class ViewHeroComponent implements OnInit {
  id:string;
  heroe: Object = {};
  user: Object = {};

  constructor(private route: ActivatedRoute, private fb: FirebaseService,
              private auth: AuthService) {
    this.id = this.route.snapshot.params['id'];
    this.fb.getHero(this.id).subscribe( data => {
       this.heroe = data;
    });
    this.user = this.auth.user;
  }

  ngOnInit() {

  }

}
