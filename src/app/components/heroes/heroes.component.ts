import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';

@Component({
   selector: "app-heroes",
   templateUrl: "./heroes.component.html",
   styleUrls: ["./heroes.component.css"]
})
export class HeroesComponent implements OnInit {
   private heroes: any[];
   constructor(private fb: FirebaseService) {
      this.fb.getHeroes().subscribe( data => {
         this.heroes = data;
      });
   }

   ngOnInit() {}

   edit(heroe) {
      console.log('El heroe a editar es: '+heroe.name + ' su id es :'+heroe.id);

   }

   deleteHeroe(id:string){
      this.fb.deleteHero(id).then(res => {
         console.log('Heroe eliminado correctamente');
      });
   }
}
