import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-add-hero',
  templateUrl: './add-hero.component.html',
  styleUrls: ['./add-hero.component.css']
})
export class AddHeroComponent implements OnInit {

  heroe: any = {};
   items: Observable<any[]>;
   constructor(private fb: FirebaseService, private router: Router) {

   }

  ngOnInit() {

  }
  addHero(){
     this.fb.addHero(this.heroe).then(res => {
        this.heroe.name = "";
        this.heroe.house = "";
        this.heroe.bio = "";
        this.router.navigate(['dashboard/heroes']);
     });
  }

}
