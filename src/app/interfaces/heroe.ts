export interface Heroe {
   id?:string,
   name?: string,
   house?: string,
   bio?: string
}
