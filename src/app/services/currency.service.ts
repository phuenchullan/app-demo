import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  private baseUrl:string;
  constructor( public http: HttpClient ) {
     this.baseUrl = "https://mindicador.cl/api/";
  }

  getCurrency(currency:string){
    return this.http.get(this.baseUrl+currency);
  }
}
