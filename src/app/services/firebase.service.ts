import { Injectable } from '@angular/core';
import { AngularFirestore, CollectionReference } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

   constructor(private db: AngularFirestore ) { }

   getHeroes(){
      return this.db.collection("heroes").snapshotChanges()
                  .pipe( map( res => {
                     let array = new Array();
                     res.forEach(item => {
                        array.push({
                           id: item['payload'].doc.id,
                           name: item['payload'].doc.data()['name'],
                           house: item['payload'].doc.data()['house'],
                           bio: item['payload'].doc.data()['bio']
                        });

                     });
                     return array;
                  }));
   }

   deleteHero(id:string){
      return this.db
         .collection("heroes")
         .doc(id)
         .delete();
   }

   updateHero(id, data){
      return this.db.collection('heroes').doc(id).update({
         'name' : data.name,
         'house': data.house,
         'bio': data.bio
      });
   }

   getHero(id:string){
      return this.db
         .collection("heroes")
         .doc(id)
         .snapshotChanges()
         .pipe(map(res => {
            let object = {
               id: res.payload.id,
               name: res.payload.data()['name'],
               house: res.payload.data()['house'],
               bio: res.payload.data()['bio']
            }
            return object;
         }));
   }

   addHero(hero){
      return this.db.collection('heroes').add(hero);
   }
}
