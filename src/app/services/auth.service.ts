import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from "firebase/app";


@Injectable({
   providedIn: "root"
})
export class AuthService {
   private authenticate: boolean;
   public user: any = {};

   constructor( public afAuth: AngularFireAuth ) {
      this.authenticate = false;
      this.afAuth.authState.subscribe( user => {
         if(! user ){
            return;
         }
         this.user.nombre = user.displayName;
         this.user.uid = user.uid;
         this.user.photoURL = user.photoURL;
         this.authenticate = true;
      });
   }

   login() {
      this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
   }
   logout() {
      this.afAuth.auth.signOut();
   }

   isAuthenticated() {
      return this.authenticate;
   }

   setAuthenticate(key: boolean) {
      this.authenticate = key;
   }

}
