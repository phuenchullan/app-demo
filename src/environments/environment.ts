// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
   production: false,
   firebase: {
      apiKey: "AIzaSyD5eeJiMoi2hEU1NU_PRSyDmuQUisk0qXA",
      authDomain: "fir-api-452a4.firebaseapp.com",
      databaseURL: "https://fir-api-452a4.firebaseio.com",
      projectId: "fir-api-452a4",
      storageBucket: "fir-api-452a4.appspot.com",
      messagingSenderId: "235155640436"
   }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
